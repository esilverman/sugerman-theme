<?php
/**
 * @package Decubing
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="google-site-verification" content="6OdPlqn8htkdxY6hot3RFgnLg6SPhkXETiLjW-7p_F8" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.static.ico" type="image/x-icon">


<!-- <link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css"> -->


<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<!-- Include Print CSS -->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="print" />
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>


<?php wp_head(); ?>
<?php

$the_title = strtolower( trim( wp_title("",false) ) );

if ($the_title == "" || is_home()) { $the_title = "home";}

if ( strtolower(get_the_title($post->post_parent)) == "menu") { $the_title ="menu"; };

/* echo "<pre>".print_r($ua,true)."</pre>"; */
?>
</head>
<body id="<?php echo str_replace(" ", "_", $the_title); ?>_body" <?php body_class(); ?>>

<?php
$contactPageId = get_page_by_title( 'Contact' );
?>

<div id="wrapper">
	<!-- START :  Header -->
	<div id="header">
		<div id="header_logo">
			<a href="<?php echo home_url(); ?>"><img id="header_img" src="<?php bloginfo('template_url'); ?>/images/logo.jpg"></a>
		</div>
		<div id="email">
			Email : <a href="mailto:laurasugerman@gmail.com">LauraSugerman@gmail.com</a></br>
		</div>
		<div id="phone">New Orleans, LA &mdash; (203) 858-2259</div>
	</div> <!-- END :  Header -->
	<div id="content" role="main">
