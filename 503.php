<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>Pizza Delicious</title>
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<link href='http://fonts.googleapis.com/css?family=Josefin+Slab:400,700' rel='stylesheet' type='text/css'>

	<style type='text/css'>
		body {
			background: #f7e907;
			text-align:center;
			margin-top:40px;
			color:black;
			font-family: "Helvetica Neue", Helvetica, sans-serif;
			text-transform: uppercase;
		}
		input {	padding: 3px;margin: 5px;}

	</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35451104-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
	<img src="http://pizzadelicious.com/wp-content/themes/pizzad-theme/images/bg-maintenance.jpg" />
	<p>In the meantime, visit our <a href="http://pizzadelicious.blogspot.com/">blog</a> for the menu and updates.</p>
</body>
</html>

