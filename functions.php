<?php
/**
 * @package Decubing
 * @subpackage Fortier
 */

$content_width = 450;

automatic_feed_links();

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
	));
}



function permalink_li_by_title($title) {
	
	$id = get_page_by_title( $title );
	
	if (is_page($title)) $itemClass = "class='current_food_page'";
	echo "<li $itemClass><a href='".get_permalink( $id )."'>$title</a></li>";
}

function print_repeater_field($fieldName, $subfields, $cols = 1, $bold = 0) {
	$rows = get_field($fieldName);
	
	if($rows)
	{
		$counter = 0;
		foreach($rows as $row) :
			$counter++; 
			if ( $counter == 1 && $cols > 1) : ?>
			<div class="repeater_row">
			<?php endif; ?>
	
				<div class="repeater_field <?php echo $fieldName;?>_repeater col_<?php echo $counter;?>">

					<?php foreach($subfields as $subfield) : ?>
						<?php if (strlen($row[$subfield]) > 0 ) :
							$subfield_content = $row[$subfield];
							if ($bold && strpos($subfield_content,",") > 0) { $subfield_content = bold_first_item($subfield_content); }
							else if ($bold) {$subfield_content = "<b>$subfield_content</b>"; }
						?>
						<span class="subfield <?php echo $subfield; ?>"><?php echo $subfield_content; ?></span>
						<?php endif;?>
					<?php endforeach; ?>

				</div>

			<?php if ( ($counter%$cols == 0 || $counter >= count($rows)) && $cols > 1) : ?>
			</div>
			<?php	$counter = $counter % $cols;
			endif;
			
		endforeach;

	}
 
} // fn print_repeater_field

function bold_first_item($text) {
	$subfield_array = explode(',',$text);
	$subfield_array[0] = "<b>".$subfield_array[0]."</b>";
	return implode(',',$subfield_array);
}

/**
 * Custom class for the WP 'body_class()' function
 * updated: 4/15/10
 */
function dbdb_body_classes($classes) {
    global $wp_query;
    
    // if there is no parent ID and it's not a single post page, category page, or 404 page, give it
    // a class of "parent-page"
    if( $wp_query->post->post_parent < 1  && !is_single() && !is_archive() && !is_404() ) {
        $classes[] = 'parent-page';
    };
    
    // if the page/post has a parent, it's a child, give it a class of its parent name
    if($wp_query->post->post_parent > 0 ) {
        $parent_title = get_the_title($wp_query->post->post_parent);
        $parent_title = preg_replace('#\s#','-', $parent_title);
        $parent_title = strtolower($parent_title);
        $classes[] = 'parent-pagename-'.$parent_title;
    };
    
    // add a class = to the name of post or page
    $postName = $wp_query->queried_object->post_name;
    $classes[] = $parent_title=="menu" ? "$postName-menu" : $postName;
    
    return array_unique($classes);
};
add_filter('body_class','dbdb_body_classes');

// Print arrays to console
function console_json($array, $title="json array") {
	echo "<script type='text/javascript'>";
	if ($title !== 0) {
		echo "console.log('$title');";
	}
	echo	"console.log(".json_encode($array).");
	</script>";
}

// Add support for Menus
register_nav_menus( array('nav_menu' => 'Main Navigation Menu') );

// Remove P tags from the_content
/* remove_filter( 'the_content', 'wpautop' ); */

?>