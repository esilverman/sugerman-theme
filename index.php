<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header();?>

<?php wp_nav_menu(); ?>
<div id="page">
	<?php
	if (have_posts() && !is_front_page()): ?>
		<div id="post">
	<?php while (have_posts()) : the_post();
		the_content();
	?>
	<?php endwhile;?>
		</div>
	<?php endif; ?>
</div>

<?php get_footer(); ?>
